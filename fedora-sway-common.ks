# fedora-livecd-sway.ks
#
# Description:
# - Fedora Live Spin with the tiling window manager i3wm
#
# Maintainer(s):
# - Aleksei Bavshin         <alebastr89@gmail.com>
# - Jiří Konečný            <jkonecny@redhat.com>
# - Anthony Rabbito         <hello@anthonyrabbito.com>
# - Fabio Alessandro Locati <me@fale.io>

%packages
fedora-release-sway
@^sway-desktop-environment
@firefox
@swaywm-extended

# To be able to show installation instructions on background
nwg-wrapper
%end
